#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models


class Participant(models.Model):
    class Meta:
        verbose_name = u"Участник"
        verbose_name_plural = u"Участники"

    name = models.CharField(
        verbose_name = u"имя",
        max_length = 200,
        unique = True
    )

    def __unicode__(self):
        return self.name


class Tournament(models.Model):
    class Meta:
        verbose_name = u"Турнир"
        verbose_name_plural = u"Турниры"

    ROUND_ROBIN_TOURNAMENT = 1
    PLAYOFF_TOURNAMENT = 2
    ROUND_ROBIN_AND_PLAYOFF_TOURNAMENT = 3

    POINTS_FOR_WIN = 3
    POINTS_FOR_DRAW = 1

    TOURNAMENT_TYPES = (
        (ROUND_ROBIN_TOURNAMENT, u"круговая система"),
        (PLAYOFF_TOURNAMENT, u"олимпийская система"),
        (ROUND_ROBIN_AND_PLAYOFF_TOURNAMENT, u"круговая и олимпийская системы"),
    )

    name = models.CharField(
        verbose_name = u"название",
        max_length = 200,
        unique = True,
    )
    type = models.PositiveSmallIntegerField(
        verbose_name = u"тип",
        choices = TOURNAMENT_TYPES,
        blank = False,
        default = ROUND_ROBIN_TOURNAMENT
    )

    def __unicode__(self):
        return self.name


class Group(models.Model):
    class Meta:
        verbose_name = u"Группа"
        verbose_name_plural = u"Группы"

    ROUND_ROBIN_GROUP_SEASON = 1
    ROUND_ROBIN_MAIN_SEASON = 2
    PLAYOFF_REGULAR_SEASON = 3
    PLAYOFF_MAIN_SEASON = 4

    GROUP_TYPES = (
        (ROUND_ROBIN_GROUP_SEASON, u"групповой сезон кругового турнира"),
        (ROUND_ROBIN_MAIN_SEASON, u"основной сезон кругового турнира"),
        (PLAYOFF_REGULAR_SEASON, u"регулярный сезон олимпийской системы"),
        (PLAYOFF_MAIN_SEASON, u"основной сезон олимпийской системы"),
    )

    name = models.CharField(
        verbose_name = u"название",
        max_length = 200,
    )
    type = models.PositiveSmallIntegerField(
        verbose_name = u"тип",
        choices = GROUP_TYPES,
        blank = False,
    )
    level = models.PositiveSmallIntegerField(verbose_name = u"уровень")
    tournament = models.ForeignKey(
        Tournament,
        verbose_name = u"чемпионат",
        on_delete = models.CASCADE
    )
    participants = models.ManyToManyField(
        Participant,
        verbose_name = u"участники"
    )

    def __unicode__(self):
        return u"{}: группа {}".format(self.tournament.name, self.name)


class Match(models.Model):
    class Meta:
        verbose_name = u"Матч"
        verbose_name_plural = u"Матчи"

    group = models.ForeignKey(Group, verbose_name = u"группа")

    participant1 = models.ForeignKey(
        Participant,
        verbose_name = u"первый участник",
        related_name = u"match_participant1",
        null = True, blank = True
    )
    participant2 = models.ForeignKey(
        Participant,
        verbose_name = u"второй участник",
        related_name = u"match_participant2",
        null = True, blank = True
    )
    date = models.DateField(verbose_name = u"дата", null = True, blank = True)
    level = models.PositiveSmallIntegerField(
        verbose_name = u"уровень",
        null = True, blank = True, default = 0
    )
    number = models.PositiveSmallIntegerField(
        verbose_name = u"порядковый номер в уровне",
        null = True, blank = True, default = 0
    )
    participant1_score = models.PositiveSmallIntegerField(
        verbose_name = u"очки первого участника",
        null = True, blank = True
    )
    participant2_score = models.PositiveSmallIntegerField(
        verbose_name = u"очки второго участника",
        null = True, blank = True
    )
    winner = models.ForeignKey(
        Participant,
        verbose_name = u"победитель",
        related_name = u"match_winner",
        null = True, blank = True
    )

    def __unicode__(self):
        date = str(self.date) if self.date else u'дата не назначена'
        name1 = self.participant1.name if self.participant1 else u"неизвестно"
        name2 = self.participant2.name if self.participant2 else u"неизвестно"

        return u"{}: {} - {} ({})".format(
            self.group.tournament.name,
            name1,
            name2,
            date,
        )
