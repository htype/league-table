from django.contrib import admin

from league_table.models import Participant, Tournament, Group, Match

admin.site.register(Participant)
admin.site.register(Tournament)
admin.site.register(Group)
admin.site.register(Match)
