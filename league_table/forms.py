#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm

from league_table.models import Tournament, Participant, Group, Match
from league_table.league_table_generator import get_round_robin_group_table
from league_table.league_table_generator import get_playoff_tournament_table
from league_table.league_table_generator import get_playoff_and_round_robin_table

class TournamentCreateForm(ModelForm):
    class Meta:
        model = Tournament
        fields = ["name", "type"]
        widgets = {
            "type": forms.RadioSelect()
        }

    number_of_groups = forms.IntegerField(min_value = 1, initial = 1)
    participants = forms.ModelMultipleChoiceField(
        queryset = Participant.objects.all(),
        to_field_name = "pk",
    )

    def create_group(
        self, name_group, group, tournament, participants
    ):
        new_group = Group.objects.create(
            name = name_group,
            level = group["level"],
            tournament = tournament,
            type = group["type"]
        )
        for participant in participants:
            if participant:
                new_group.participants.add(participant)

        i = 1
        matches = group["matches"]
        for season in matches:
            matches_in_season = matches[season]
            for match in matches_in_season:
                new_match = Match.objects.create(
                    group = new_group,
                    participant1 = match[0],
                    participant2 = match[1],
                    level = season,
                    number = i,
                )
                i += 1


    def save(self, commit = True):
        left_from_group = 2
        instance = super(TournamentCreateForm, self).save(commit = False)
        instance.save()
        tournament_type = self.cleaned_data["type"]
        number_of_groups = self.cleaned_data["number_of_groups"]
        participants = list(self.cleaned_data["participants"])
        print("participants: {}".format(participants))
        if tournament_type == Tournament.ROUND_ROBIN_TOURNAMENT:
            table = get_round_robin_group_table(
                participants, number_of_groups, left_from_group
            )
        elif tournament_type == Tournament.PLAYOFF_TOURNAMENT:
            table = get_playoff_tournament_table(participants)
        elif tournament_type == Tournament.ROUND_ROBIN_AND_PLAYOFF_TOURNAMENT:
            table = get_playoff_and_round_robin_table(
                participants, number_of_groups, left_from_group
            )
        else:
            return None

        for group in table:
            self.create_group(group, table[group], instance, participants)

        return instance
