# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-23 10:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('league_table', '0003_match_group'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='data',
            field=models.DateField(verbose_name='\u0434\u0430\u0442\u0430'),
        ),
    ]
