import math
import random

from league_table.models import Group

def turn_counterclockwise(round):
    middle = len(round) // 2
    turned_round = [round[0]] + [round[-1]] + round[1:-1]
    return turned_round


def form_pairs(round_vector):
    count = len(round_vector)
    if (count % 2 != 0):
        return None

    middle = count // 2
    pairs = [
        (round_vector[i], round_vector[count - i - 1])
    for i in range(middle) if not (bool(round_vector[i]) ^ bool(round_vector[count - i - 1]))]
    return pairs


def split_to_groups(teams, number_of_groups):
    minimum_group_size = 2
    number_of_teams = len(teams)
    if (number_of_groups < 1) or (number_of_teams < number_of_groups * 2):
        return None
    if number_of_groups == 1:
        return [teams]

    teams_per_group = number_of_teams // number_of_groups
    groups = [[] for i in range(number_of_groups)]
    pool = teams
    for i in range(number_of_teams):
        pool, team = get_random_teams(pool, 1)
        groups[i % number_of_groups] += team
    print("groups:{}".format(groups))
    return groups

def get_group_letter(number):
    if (0 <= number < 23):
        return chr(ord('A') + number)
    return None


def get_round_robin_group(teams_in_group, level, is_main):
    group = {
        "type": (Group.ROUND_ROBIN_MAIN_SEASON if is_main
                    else Group.ROUND_ROBIN_GROUP_SEASON),
        "level": level,
        "matches": {
            0: round_robin_table(teams_in_group)
        }
    }
    return group


def get_round_robin_group_table(
    teams, number_of_groups = 1, left_from_group = 1
):
    table = {}

    table['main season'] = {
        "level": 0,
        "type": Group.ROUND_ROBIN_MAIN_SEASON
    }
    if (number_of_groups > 1):
        groups = split_to_groups(teams, number_of_groups)
        number_of_left_teams = number_of_groups * left_from_group
        number_of_rounds = number_of_left_teams
        if number_of_rounds % 2 == 0:
            number_of_rounds -= 1
        number_of_pairs = number_of_rounds * (number_of_left_teams - 1) // 2
        main_season = [[None, None] for i in range(number_of_pairs)]
        table['main season']['matches'] = {
            0: main_season
        }

        group_number = 0
        for teams_in_group in groups:
            table[group_number] = get_round_robin_group(
                teams_in_group, 1, False
            )
            group_number += 1
    else:
        table['main season']['matches'] = {
            0: round_robin_table(teams)
        }

    return table

def round_robin_table(teams):
    count = len(teams)
    if count % 2 == 0:
        rounds = count - 1
    else:
        rounds = count
        teams.append(None)
        count += 1

    round_vector = teams
    tournament_table = form_pairs(round_vector)
    for i in range(rounds - 1):
        round_vector = turn_counterclockwise(round_vector)
        tournament_table += form_pairs(round_vector)

    return tournament_table


def get_random_teams(teams, count):
    if count > len(teams):
        return []
    team_pool = teams
    random_teams = []
    for i in range(count):
        choice = random.choice(team_pool)
        random_teams += [choice]
        team_pool.remove(choice)
    return team_pool, random_teams


def get_playoff_pairs(teams, is_random = False):
    if is_random:
        random.shuffle(teams)
    team_count = len(teams)
    pairs = []
    for i in range(0, team_count, 2):
        pairs += [(teams[i], teams[i+1])]
    return pairs


def get_playoff_tournament_table(teams):
    count = len(teams)
    base = int(math.log(count, 2))
    base_count = 2 ** base
    delta = count - base_count
    print("delta: {}; base: {}; count: {}".format(delta, base, count))
    rounds = {}
    if delta > 0:
        teams, regular_season_teams = get_random_teams(teams, 2 * delta)
        rounds["regular_season"] = {
            "type": Group.PLAYOFF_REGULAR_SEASON,
            "level": 0,
            "matches": {
                0: get_playoff_pairs(regular_season_teams, True)
            }
        }
        teams += [None] * delta

    team_count_in_round = base_count // 2
    stage = int(math.log(team_count_in_round, 2))
    matches = {}
    matches[stage] = get_playoff_pairs(teams, team_count_in_round)
    for i in range(base - 1):
        team_count_in_round //= 2
        stage -= 1
        matches[stage] = [(None, None)] * team_count_in_round

    rounds['main season'] = {
        "type": Group.PLAYOFF_MAIN_SEASON,
        "level": 1,
        "matches": matches,
    }

    return rounds


def get_playoff_and_round_robin_table(teams, number_of_groups, left_from_group):
    table = {}
    if (number_of_groups > 1):
        groups = split_to_groups(teams, number_of_groups)

        group_number = 0
        for teams_in_group in groups:
            table[group_number] = get_round_robin_group(
                teams_in_group, 1, False
            )
            group_number += 1
    else:
        table["qualifying"] = get_round_robin_group(
            teams, 1, False
        )
    left_teams = [None] * (number_of_groups * left_from_group)
    playoff_table = get_playoff_tournament_table(left_teams)
    table.update(playoff_table)
    print("general table: {}".format(table))
    return table
