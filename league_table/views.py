#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
from operator import itemgetter

from django.http import Http404
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import DeleteView, CreateView
from django.views.generic import TemplateView
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator
from django.db.models import Q, F
from django.db.models import Count, Sum

from league_table.models import Tournament, Match, Group, Participant
from league_table.forms import TournamentCreateForm

class TournamentListView(ListView):
    model = Tournament
    context_object_name = u"tournaments"
    template_name = u"league_table/tournament_list.html"

    @method_decorator(staff_member_required)
    def dispatch(self, *args, **kwargs):
        return super(TournamentListView, self).dispatch(*args, **kwargs)


class TournamentDeleteView(DeleteView):
    model = Tournament
    success_url = reverse_lazy(u"tournament_list_url")

    def get(self, request, *args, **kwargs):
        raise Http404()

class TournamentCreateView(CreateView):
    model = Tournament
    form_class = TournamentCreateForm
    success_url = reverse_lazy(u"tournament_list_url")
    template_name = u"league_table/tournament_create.html"


class TournamentInfoView(TemplateView):
    model = Tournament
    success_url = reverse_lazy(u"tournament_info_url")
    template_name = u"league_table/tournament_info.html"

    def _get_tournament_table(self, tournament, group_name):
        group = Group.objects.filter(
            tournament = tournament,
            name = group_name,
        )
        matches = (Match.objects.filter(group = group)
                                .filter(participant1__isnull = False)
                                .filter(participant2__isnull = False))
        participants_pk = (
            set(matches.values_list("participant1", flat = True)) |
            set(matches.values_list("participant2", flat = True))
        )
        participants = Participant.objects.filter(
            pk__in = participants_pk
        )

        tournament_table = []
        for participant in participants:
            games_with_participant = matches.filter(
                Q(participant1_score__isnull = False) &
                (Q(participant1 = participant) |
                 Q(participant2 = participant))
            )
            number_of_games = games_with_participant.count()
            winnings = games_with_participant.filter(
                winner = participant
            ).count()
            losses = (games_with_participant.exclude(winner = participant)
                .exclude(winner__isnull = True)
            ).count()
            draws = number_of_games - (losses + winnings)
            points = (winnings * Tournament.POINTS_FOR_WIN +
                      draws * Tournament.POINTS_FOR_DRAW)
            participant1_goal_delta = games_with_participant.filter(
                participant1 = participant
            ).aggregate(
                goal_delta = (Sum("participant1_score") -
                                Sum("participant2_score"))
            )["goal_delta"] or 0
            participant2_goal_delta = games_with_participant.filter(
                participant2 = participant
            ).aggregate(
                goal_delta = (Sum("participant2_score") -
                                Sum("participant1_score"))
            )["goal_delta"] or 0

            table_item = {
                "participant": participant.name,
                "number_of_games": number_of_games,
                "winnings": winnings,
                "losses": losses,
                "points": points,
                "goal_delta": (participant1_goal_delta +
                                participant2_goal_delta),
            }
            tournament_table += [table_item]

        sorted_tournament_table = sorted(
            tournament_table,
            key = itemgetter("points", "goal_delta"),
            reverse = True
        )
        return sorted_tournament_table

    def _get_playoff_level_name(self, number):
        if (number == 0):
            return u"Финал"
        elif (number == 1):
            return u"Полуфинал"
        elif (number == 2):
            return u"Четвертьфинал"
        else:
            return u"1/{}".format(2 ** number)

    def _get_playoff_main_season(self, tournament_groups):
        main_season_group = tournament_groups.get(
            type = Group.PLAYOFF_MAIN_SEASON
        )
        main_group_matches = Match.objects.filter(
            group = main_season_group
        )
        playoff_levels = int(math.log(main_group_matches.count() + 1, 2))
        playoff_main_season = []
        for level in range(playoff_levels):
            current_level_matches = {
                "name": self._get_playoff_level_name(level),
                "rank": 2 ** level,
                "matches": main_group_matches.filter(
                    level = level
                ).order_by("number")
            }
            playoff_main_season += [current_level_matches]

        playoff_main_season = sorted(
            playoff_main_season,
            key = itemgetter("rank"),
            reverse = True
        )
        return playoff_main_season


    def get_tournament_groups(self, tournament):
        groups = {}
        tournament_groups = Group.objects.filter(tournament = tournament)

        if (tournament.type == Tournament.ROUND_ROBIN_TOURNAMENT or
            tournament.type == Tournament.ROUND_ROBIN_AND_PLAYOFF_TOURNAMENT):
            groups["round_robin_group_season"] = tournament_groups.filter(
                type = Group.ROUND_ROBIN_GROUP_SEASON
            )
            groups["round_robin_main_season"] = tournament_groups.filter(
                type = Group.ROUND_ROBIN_MAIN_SEASON
            )

        if (tournament.type == Tournament.PLAYOFF_TOURNAMENT or
            tournament.type == Tournament.ROUND_ROBIN_AND_PLAYOFF_TOURNAMENT):
            groups["playoff_regular_season"] = tournament_groups.filter(
                type = Group.PLAYOFF_REGULAR_SEASON
            )
            groups["playoff_main_season"] = self._get_playoff_main_season(
                tournament_groups
            )
        return groups

    def get_context_data(self, **kwargs):
        context = super(TournamentInfoView, self).get_context_data()
        pk = kwargs["pk"]
        tournament = Tournament.objects.get(pk = pk)
        groups = Group.objects.filter(tournament = tournament)

        if (tournament.type == Tournament.ROUND_ROBIN_TOURNAMENT):
            table = self._get_tournament_table(tournament, "main season")
            context["tournament_table"] = table

        matches = Match.objects.filter(
            Q(group__in = groups) &
            Q(participant1__isnull = False) & Q(participant2__isnull = False)
        ).order_by('number')

        context["groups"] = self.get_tournament_groups(tournament)
        context["tournament"] = tournament
        context["matches"] = matches
        context["const"] = Tournament
        return context
