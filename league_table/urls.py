"""league_table URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings

from league_table.views import TournamentListView
from league_table.views import TournamentDeleteView
from league_table.views import TournamentCreateView
from league_table.views import TournamentInfoView

urlpatterns = [
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^$', TournamentListView.as_view(), name = u"tournament_list_url"),
    url(r'^tournament/delete/(?P<pk>\w+)/$', TournamentDeleteView.as_view(), name = u"tournament_delete_url"),
    url(r'^tournament/create/$', TournamentCreateView.as_view(), name = u"tournament_create_url"),
    url(r'^tournament/info/(?P<pk>\w+)/$', TournamentInfoView.as_view(), name = u"tournament_info_url"),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root = settings.STATIC_ROOT)
